package com.kodcu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author hakdogan (hakdogan@kodcu.com)
 * Created on 12.11.2018
 */

@RestController
@EnableAutoConfiguration
public class WelcomeController {

    @GetMapping("/")
    public String welcomePage(){
        return "Selam millet ben geldim...";
    }

    public static void main(String[] args){
        SpringApplication.run(WelcomeController.class, args);
    }
}
