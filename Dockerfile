FROM ubuntu:latest

ARG USER=1001
ARG APPDIR="/deployments"

LABEL maintainer="Huseyin Akdogan <hakdogan@kodcu.com>" \
      io.k8s.description="First Pipeline Buil." \
      io.k8s.display-name="SpringBoot" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="builder,java,maven"


COPY pom.xml $APPDIR/pom.xml
COPY src/ $APPDIR//src/

RUN useradd $USER \
    && chown $USER:$USER $APPDIR \
    && addgroup $USER $USER \
    && chmod 777 -R $APPDIR

RUN apt-get update -y && \
    apt-get install -y software-properties-common && \
    echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections && \
    add-apt-repository -y ppa:webupd8team/java && \
    apt-get update && \
    apt-get install -y oracle-java8-installer && \
    apt-get install oracle-java8-set-default && \
    rm -rf /var/cache/oracle-jdk8-installer && \
    apt-get install maven -y && \
    rm -rf /var/lib/apt/lists/*

WORKDIR $APPDIR

RUN mvn clean install

EXPOSE 8080

USER $USER

CMD ["sh","-c", "java -Dnetworkaddress.cache.ttl=60 -jar target/spring-boot-1.0-SNAPSHOT.jar"]